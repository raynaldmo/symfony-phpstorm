<?php
/**
 * Created by PhpStorm.
 * User: raynald
 * Date: 10/3/19
 * Time: 1:38 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MovieRepository")
 * @ORM\Table(name="movie")
 */
class Movie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $characterName;

    /**
     * @ORM\Column(type="integer")
     */
    private $rating;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isMainCharacter;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $releasedAt;


    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    public function getCharacterName()
    {
        return $this->characterName;
    }

    /**
     * @param string $characterName
     */
    public function setCharacterName($characterName)
    {
        $this->characterName = $characterName;
    }

    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param integer $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    public function getisMainCharacter()
    {
        return $this->isMainCharacter;
    }

    /**
     * @param boolean $isMainCharacter
     */
    public function setIsMainCharacter($isMainCharacter)
    {
        $this->isMainCharacter = $isMainCharacter;
    }

    public function getReleasedAt()
    {
        return $this->releasedAt;
    }

    /**
     * @param \DateTime $releasedAt
     */
    public function setReleasedAt($releasedAt)
    {
        $this->releasedAt = $releasedAt;
    }

}