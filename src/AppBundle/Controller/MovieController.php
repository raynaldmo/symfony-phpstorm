<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Movie;
use AppBundle\Form\MovieType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MovieController extends Controller
{
    /**
     * @Route("/movies/new", name="movies_new")
     */
    public function newAction(Request $request)
    {
        $movie = new Movie();
        $form = $this->createForm(new MovieType(), $movie);


        $form->handleRequest($request);
        if ($form->isValid()) {
            // Save data to database
            $em = $this->getDoctrine()->getManager();
            $em->persist($movie);
            $em->flush();

            $status = 'success';
            $this->addFlash($status, 'Movie ' . $movie->getCharacterName() . ' saved.');

            return $this->redirectToRoute('movies_list', array());
        }

        return $this->render('movie/new.html.twig', array(
          'quote' => $this->get('app.quote_generator')->getRandomQuote(),
          'form' => $form->createView()
        ));
    }

    /**
     * @Route("/movies", name="movies_list")
     */
    public function listAction()
    {
        $movies = $this->getDoctrine()
          ->getRepository('AppBundle:Movie')
          ->findAll();

        return $this->render('movie/list.html.twig', array(
          'movies' => $movies,
        ));
    }
}
